
//  2016 @burakerken

// Nrf24l01 2.4 wireless transceiver code for stm32 processors in shockburst mode ()
// the code works on all stm32 processors with an spi module.
// for understanding the principal :https://lastminuteengineers.com/nrf24l01-arduino-wireless-communication/
// Only change the following 6 lines according to your chips spi pins.

sbit CE_pin at GPIOA_ODR.B1;
sbit CSN_pin at GPIOE_ODR.B3;
sbit SCK_pin at GPIOA_ODR.B5;
sbit MOSI_pin at GPIOA_ODR.B7;
sbit MISO_pin at GPIOA_IDR.B6;
sbit Led at GPIOD_ODR.B12;

#define R_REGISTER_cmd 0x00
#define W_REGISTER_cmd 0x20
#define R_RX_PL_WID_cmd 0x60
#define R_RX_PAYLOAD_cmd 0x61
#define W_TX_PAYLOAD_cmd 0xA0
#define W_ACK_PAYLOAD_cmd 0xA8
#define W_TX_PAYLOAD_NO_ACK_cmd 0xB0
#define FLUSH_TX_cmd 0xE1
#define FLUSH_RX_cmd 0xE2
#define REUSE_TX_PL_cmd 0xE3
#define NOP_cmd 0xFF

#define CONFIG_reg 0x00
#define EN_AA_reg 0x01
#define EN_RXADDR_reg 0x02
#define SETUP_AW_reg 0x03
#define SETUP_RETR_reg 0x04
#define RF_CH_reg 0x05
#define RF_SETUP_reg 0x06
#define STATUS_reg 0x07
#define OBSERVE_TX_reg 0x08
#define RPD_reg 0x09
#define RX_ADDR_P0_reg 0x0A
#define RX_ADDR_P1_reg 0x0B
#define RX_ADDR_P2_reg 0x0C
#define RX_ADDR_P3_reg 0x0D
#define RX_ADDR_P4_reg 0x0E
#define RX_ADDR_P5_reg 0x0F
#define TX_ADDR_reg 0x10
#define RX_PW_P0_reg 0x11
#define RX_PW_P1_reg 0x12
#define RX_PW_P2_reg 0x13
#define RX_PW_P3_reg 0x14
#define RX_PW_P4_reg 0x15
#define RX_PW_P5_reg 0x16
#define FIFO_STATUS_reg 0x17
#define DYNPD_reg 0x1C
#define FEATURE_reg 0x1D

char *consoleMessage = "Console Message Will be sent to PC";

void register_write(unsigned int reg, unsigned int value)
{
  SPI1_Init();
  CSN_pin = 0;
  SPI1_Write((W_REGISTER_cmd | reg)); //  write to 'reg'
  SPI1_Write(value);                  //  'value' setted.
  CSN_pin = 1;

  //For Debug
  consoleMessage = "Yazilan Register= ";
  UART1_Write_Text(consoleMessage);
  delay_ms(10);
  UART1_Write(reg);
  UART1_Write(10);
  delay_ms(10);
  consoleMessage = "Yazilan Deger= ";
  UART1_Write_Text(consoleMessage);
  delay_ms(10);
  UART1_Write(value);
  UART1_Write(10);
  delay_ms(10);
  // For Debug
}

unsigned char register_read(unsigned int reg)
{
  unsigned int value = 0;

  CSN_pin = 0;
  SPI1_Write((R_REGISTER_cmd | reg));
  value = Spi1_Read(0);
  delay_us(10);
  CSN_pin = 1;
  delay_us(8);

  // For Debug
  consoleMessage = "Register Oku =";
  UART1_Write_Text(consoleMessage);
  UART1_Write(reg);
  UART1_Write(10);
  UART1_Write(value);
  UART1_Write(10);
  // For Debug
  return value;
}

void write_command(unsigned int cmd)
{
  SPI1_Init();
  CSN_pin = 0;
  SPI1_Write(cmd);
  CSN_pin = 1;
  delay_us(8);
}

void flush_TX_RX()
{
  SPI1_Init();
  register_write(STATUS_reg, 0x70);
  write_command(FLUSH_TX_cmd);
  delay_us(10);
  write_command(FLUSH_RX_cmd);
}

void get_Status_Reg()
{
  unsigned int status;

  CSN_pin = 0;
  status = SPI1_Read(STATUS_reg);
  CSN_pin = 1;
  delay_us(8);

  consoleMessage = "Status Register = ";
  UART1_Write_Text(consoleMessage);
  UART1_Write(status);
  UART1_Write(10);
}

void set_TX_RX_address(unsigned char *addr, unsigned char bytes, unsigned char reg)
{
  unsigned char n = 0;

  CSN_pin = 0;
  SPI1_write((reg | W_REGISTER_cmd));
  for (n = 0; n < bytes; n++)
  {
    SPI1_write(addr[n]);
  }
  CSN_pin = 1;
  delay_us(8);
}

void send_data(unsigned int data)
{

  flush_TX_RX();

  CSN_pin = 0;
  SPI1_write(W_TX_PAYLOAD_cmd);

  SPI1_write(data);
  SPI1_write(data);
  SPI1_write(data);

  CSN_pin = 1;
  delay_us(8);

  CE_pin = 1;
  delay_us(60);
  CE_pin = 0;
}

void nrF24L01_init_TX()
{
  unsigned char address[5] = {0x78, 0x78, 0x78, 0x78, 0x78};

  register_write(CONFIG_reg, 0x0E); // pwr_up =1 ,  enable CRC(2 bytes),  Prim:TX

  set_TX_RX_address(address, 5, TX_ADDR_reg);

  set_TX_RX_address(address, 5, RX_ADDR_P0_reg);

  register_write(EN_AA_reg, 0x01);

  register_write(SETUP_RETR_reg, 0x1A);
}

void main()
{
  SPI1_Init();
  UART1_Init_Advanced(9600, _UART_8_BIT_DATA, _UART_NOPARITY, _UART_ONE_STOPBIT, &_GPIO_MODULE_USART1_PB67);
  GPIO_Digital_Output(&GPIOD_BASE, _GPIO_PINMASK_12);
  GPIO_Digital_Output(&GPIOA_BASE, _GPIO_PINMASK_1 | _GPIO_PINMASK_5 | _GPIO_PINMASK_7);
  GPIO_Digital_Output(&GPIOE_BASE, _GPIO_PINMASK_3);
  GPIO_Digital_Input(&GPIOA_BASE, _GPIO_PINMASK_6);
  GPIO_Digital_Input(&GPIOA_IDR, _GPIO_PINMASK_0);

  consoleMessage = "NRF-2016 @burakerken";
  UART1_Write_Text(consoleMessage);
  delay_ms(10);
  UART1_Write(10);
  delay_ms(10);

  CE_pin = 0;
  CSN_pin = 0;
  SCK_pin = 0;
  MOSI_pin = 0;
  nrF24L01_init_TX();

  while (1)
  {
    send_data(30);
    delay_ms(1500);

  }
}