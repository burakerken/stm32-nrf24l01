#line 1 "C:/Users/lenovo/Desktop/stm-nrf/tx/tx.c"
sbit CE_pin at GPIOA_ODR.B1;
sbit CSN_pin at GPIOE_ODR.B3;
sbit SCK_pin at GPIOA_ODR.B5;
sbit MOSI_pin at GPIOA_ODR.B7;
sbit MISO_pin at GPIOA_IDR.B6;
sbit Led at GPIOD_ODR.B12;
#line 48 "C:/Users/lenovo/Desktop/stm-nrf/tx/tx.c"
unsigned int address=0;
unsigned int k=0;
unsigned int x=0,y=0,z=0;


char *consoleMessage ="123";

void register_write(unsigned int reg, unsigned int value)
{ SPI1_Init();
 CSN_pin = 0;
 SPI1_Write(( 0x20 |reg ));
 SPI1_Write(value);
 CSN_pin = 1;


consoleMessage="Yazilan Register= ";
UART1_Write_Text(consoleMessage);
delay_ms(10);
UART1_Write(reg);
UART1_Write(10);
delay_ms(10);
consoleMessage="Yazilan Deger= ";
UART1_Write_Text(consoleMessage);
delay_ms(10);
UART1_Write(value);
UART1_Write(10);
delay_ms(10);

}

unsigned char register_read(unsigned int reg)
{
 unsigned int value = 0;

consoleMessage="Register Oku =";
UART1_Write_Text(consoleMessage);
delay_ms(10);

UART1_Write(reg);
delay_ms(10);
UART1_Write(10);
delay_ms(10);



 delay_ms(10);
 CSN_pin= 0;
 SPI1_Write((reg |  0x00 ));
 value = Spi1_Read(0);
 delay_us(10);
 CSN_pin= 1;
 delay_us(8);

UART1_Write(value);
UART1_Write(10);
return value;
}

void write_command(unsigned int cmd)
{ SPI1_Init();
 CSN_pin = 0;
 SPI1_Write(cmd);
 CSN_pin = 1;
 delay_us(8);
}

void flush_TX_RX()
{ SPI1_Init();
 register_write( 0x07 ,0x70);
 write_command( 0xE1 );
 delay_us(10);
 write_command( 0xE2 );
}

void get_Status_Reg()
{ unsigned int status ;
 CSN_pin = 0;
 delay_ms(10);
 status= SPI1_Read( 0x07 );

 CSN_pin = 1;
 delay_us(10);
 consoleMessage="Status Register = ";
 UART1_Write_Text(consoleMessage);
 delay_ms(10);

 UART1_Write(status);
 UART1_Write(10);
 UART1_Write(10);
 delay_ms(10);


}

void set_TX_RX_address(unsigned char *addr, unsigned char bytes, unsigned char reg)
{
 unsigned char n = 0;

 CSN_pin= 0;
 SPI1_write((reg |  0x20 ));
 for(n = 0; n < bytes; n++)
 {
 SPI1_write(addr[n]);
 }
 CSN_pin = 1;
 delay_us(8);
}
#line 178 "C:/Users/lenovo/Desktop/stm-nrf/tx/tx.c"
void send_data(unsigned int k)
{


 flush_TX_RX();

 CSN_pin= 0;
 SPI1_write( 0xA0 );

 SPI1_write(k);
 SPI1_write(k);
 SPI1_write(k);

 CSN_pin= 1;
 delay_us(8);

 CE_pin = 1;
 delay_us(60);
 CE_pin = 0;

}


void nrF24L01_init_TX()
{
 unsigned char address[5] = {0x78, 0x78, 0x78, 0x78, 0x78};

 register_write( 0x00 , 0x0E);

 set_TX_RX_address(address, 5,  0x10 );

 set_TX_RX_address(address, 5,  0x0A );

 register_write( 0x01 ,0x01);

 register_write( 0x04 ,0x1A);


}


void main() {
SPI1_Init();
UART1_Init_Advanced(9600,_UART_8_BIT_DATA, _UART_NOPARITY, _UART_ONE_STOPBIT, &_GPIO_MODULE_USART1_PB67);
GPIO_Digital_Output(&GPIOD_BASE, _GPIO_PINMASK_12 );
GPIO_Digital_Output(&GPIOA_BASE, _GPIO_PINMASK_1 |_GPIO_PINMASK_5|_GPIO_PINMASK_7);
GPIO_Digital_Output(&GPIOE_BASE, _GPIO_PINMASK_3);
GPIO_Digital_Input(&GPIOA_BASE, _GPIO_PINMASK_6);
GPIO_Digital_Input(&GPIOA_IDR, _GPIO_PINMASK_0);


consoleMessage="NRF-2016";
UART1_Write_Text(consoleMessage);
delay_ms(10);
UART1_Write(10);
delay_ms(10);


 CE_pin = 0;
 CSN_pin = 0;
 SCK_pin = 0;
 MOSI_pin = 0;
 delay_ms(900);
nrF24L01_init_TX();
delay_ms(1000);

while(1)
{
 send_data(30);

 delay_ms(1500);
#line 255 "C:/Users/lenovo/Desktop/stm-nrf/tx/tx.c"
}

}
