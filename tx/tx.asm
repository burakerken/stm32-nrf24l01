_register_write:
;tx.c,55 :: 		void register_write(unsigned int reg, unsigned int value)
SUB	SP, SP, #20
STR	LR, [SP, #0]
STRH	R0, [SP, #4]
STRH	R1, [SP, #8]
;tx.c,56 :: 		{ SPI1_Init();
BL	_SPI1_Init+0
;tx.c,57 :: 		CSN_pin = 0;
MOVS	R3, #0
SXTB	R3, R3
MOVW	R2, #lo_addr(GPIOE_ODR+0)
MOVT	R2, #hi_addr(GPIOE_ODR+0)
STR	R3, [R2, #0]
;tx.c,58 :: 		SPI1_Write((W_REGISTER_cmd|reg ));   //  YAZMA OLACAGI SOYLENIP- HANGI REG OLD. BELİRTILDI
LDRH	R2, [SP, #4]
ORR	R2, R2, #32
UXTH	R0, R2
BL	_SPI1_Write+0
;tx.c,59 :: 		SPI1_Write(value);                //   ISTENEN DEGER YAZILDI.
LDRH	R0, [SP, #8]
BL	_SPI1_Write+0
;tx.c,60 :: 		CSN_pin = 1;
MOVS	R3, #1
SXTB	R3, R3
MOVW	R2, #lo_addr(GPIOE_ODR+0)
MOVT	R2, #hi_addr(GPIOE_ODR+0)
STR	R3, [R2, #0]
;tx.c,63 :: 		src="Yazilan Register= "; // delay silindi
MOVW	R3, #lo_addr(?lstr2_tx+0)
MOVT	R3, #hi_addr(?lstr2_tx+0)
MOVW	R2, #lo_addr(_src+0)
MOVT	R2, #hi_addr(_src+0)
STR	R2, [SP, #16]
STR	R3, [R2, #0]
;tx.c,64 :: 		UART1_Write_Text(src);
LDR	R2, [R2, #0]
MOV	R0, R2
BL	_UART1_Write_Text+0
;tx.c,65 :: 		delay_ms(10);
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_register_write0:
SUBS	R7, R7, #1
BNE	L_register_write0
NOP
NOP
NOP
NOP
;tx.c,66 :: 		UART1_Write(reg);
LDRH	R0, [SP, #4]
BL	_UART1_Write+0
;tx.c,67 :: 		UART1_Write(10);
MOVS	R0, #10
BL	_UART1_Write+0
;tx.c,68 :: 		delay_ms(10);
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_register_write2:
SUBS	R7, R7, #1
BNE	L_register_write2
NOP
NOP
NOP
NOP
;tx.c,69 :: 		src="Yazilan Deger= "; // delay silindi
MOVW	R3, #lo_addr(?lstr3_tx+0)
MOVT	R3, #hi_addr(?lstr3_tx+0)
MOVW	R2, #lo_addr(_src+0)
MOVT	R2, #hi_addr(_src+0)
STR	R3, [R2, #0]
;tx.c,70 :: 		UART1_Write_Text(src);
LDR	R2, [SP, #16]
LDR	R2, [R2, #0]
MOV	R0, R2
BL	_UART1_Write_Text+0
;tx.c,71 :: 		delay_ms(10);
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_register_write4:
SUBS	R7, R7, #1
BNE	L_register_write4
NOP
NOP
NOP
NOP
;tx.c,72 :: 		UART1_Write(value);
LDRH	R0, [SP, #8]
BL	_UART1_Write+0
;tx.c,73 :: 		UART1_Write(10);
MOVS	R0, #10
BL	_UART1_Write+0
;tx.c,74 :: 		delay_ms(10);
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_register_write6:
SUBS	R7, R7, #1
BNE	L_register_write6
NOP
NOP
NOP
NOP
;tx.c,76 :: 		}
L_end_register_write:
LDR	LR, [SP, #0]
ADD	SP, SP, #20
BX	LR
; end of _register_write
_register_read:
;tx.c,78 :: 		unsigned char register_read(unsigned int reg)
; reg start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
UXTH	R7, R0
; reg end address is: 0 (R0)
; reg start address is: 28 (R7)
;tx.c,80 :: 		unsigned int value = 0;
;tx.c,82 :: 		src="Register Oku ="; // delay silindi
MOVW	R2, #lo_addr(?lstr4_tx+0)
MOVT	R2, #hi_addr(?lstr4_tx+0)
MOVW	R1, #lo_addr(_src+0)
MOVT	R1, #hi_addr(_src+0)
STR	R2, [R1, #0]
;tx.c,83 :: 		UART1_Write_Text(src);
LDR	R1, [R1, #0]
MOV	R0, R1
BL	_UART1_Write_Text+0
;tx.c,84 :: 		delay_ms(10);
STRH	R7, [SP, #4]
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_register_read8:
SUBS	R7, R7, #1
BNE	L_register_read8
NOP
NOP
NOP
NOP
LDRH	R7, [SP, #4]
;tx.c,86 :: 		UART1_Write(reg);
UXTH	R0, R7
BL	_UART1_Write+0
;tx.c,87 :: 		delay_ms(10);
STRH	R7, [SP, #4]
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_register_read10:
SUBS	R7, R7, #1
BNE	L_register_read10
NOP
NOP
NOP
NOP
LDRH	R7, [SP, #4]
;tx.c,88 :: 		UART1_Write(10);
MOVS	R0, #10
BL	_UART1_Write+0
;tx.c,89 :: 		delay_ms(10);
STRH	R7, [SP, #4]
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_register_read12:
SUBS	R7, R7, #1
BNE	L_register_read12
NOP
NOP
NOP
NOP
;tx.c,93 :: 		delay_ms(10);
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_register_read14:
SUBS	R7, R7, #1
BNE	L_register_read14
NOP
NOP
NOP
NOP
;tx.c,89 :: 		delay_ms(10);
LDRH	R7, [SP, #4]
;tx.c,94 :: 		CSN_pin= 0;
MOVS	R2, #0
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOE_ODR+0)
MOVT	R1, #hi_addr(GPIOE_ODR+0)
STR	R2, [R1, #0]
;tx.c,95 :: 		SPI1_Write((reg | R_REGISTER_cmd));
UXTH	R0, R7
; reg end address is: 28 (R7)
BL	_SPI1_Write+0
;tx.c,96 :: 		value = Spi1_Read(0);
MOVS	R0, #0
BL	_SPI1_Read+0
; value start address is: 16 (R4)
UXTH	R4, R0
;tx.c,97 :: 		delay_us(10);
MOVW	R7, #51
MOVT	R7, #0
NOP
NOP
L_register_read16:
SUBS	R7, R7, #1
BNE	L_register_read16
NOP
NOP
NOP
NOP
;tx.c,98 :: 		CSN_pin= 1;
MOVS	R2, #1
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOE_ODR+0)
MOVT	R1, #hi_addr(GPIOE_ODR+0)
STR	R2, [R1, #0]
;tx.c,99 :: 		delay_us(8);
MOVW	R7, #41
MOVT	R7, #0
NOP
NOP
L_register_read18:
SUBS	R7, R7, #1
BNE	L_register_read18
NOP
NOP
;tx.c,101 :: 		UART1_Write(value);
UXTH	R0, R4
BL	_UART1_Write+0
;tx.c,102 :: 		UART1_Write(10);
MOVS	R0, #10
BL	_UART1_Write+0
;tx.c,103 :: 		return value;
UXTB	R0, R4
; value end address is: 16 (R4)
;tx.c,104 :: 		}
L_end_register_read:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _register_read
_write_command:
;tx.c,106 :: 		void write_command(unsigned int cmd)
; cmd start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
; cmd end address is: 0 (R0)
; cmd start address is: 0 (R0)
;tx.c,107 :: 		{ SPI1_Init();
STRH	R0, [SP, #4]
BL	_SPI1_Init+0
LDRH	R0, [SP, #4]
;tx.c,108 :: 		CSN_pin = 0;
MOVS	R2, #0
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOE_ODR+0)
MOVT	R1, #hi_addr(GPIOE_ODR+0)
STR	R2, [R1, #0]
;tx.c,109 :: 		SPI1_Write(cmd);
; cmd end address is: 0 (R0)
BL	_SPI1_Write+0
;tx.c,110 :: 		CSN_pin = 1;
MOVS	R2, #1
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOE_ODR+0)
MOVT	R1, #hi_addr(GPIOE_ODR+0)
STR	R2, [R1, #0]
;tx.c,111 :: 		delay_us(8);
MOVW	R7, #41
MOVT	R7, #0
NOP
NOP
L_write_command20:
SUBS	R7, R7, #1
BNE	L_write_command20
NOP
NOP
;tx.c,112 :: 		}
L_end_write_command:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _write_command
_flush_TX_RX:
;tx.c,114 :: 		void flush_TX_RX()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;tx.c,115 :: 		{ SPI1_Init();
BL	_SPI1_Init+0
;tx.c,116 :: 		register_write(STATUS_reg,0x70);
MOVS	R1, #112
MOVS	R0, #7
BL	_register_write+0
;tx.c,117 :: 		write_command(FLUSH_TX_cmd);
MOVS	R0, #225
BL	_write_command+0
;tx.c,118 :: 		delay_us(10);
MOVW	R7, #51
MOVT	R7, #0
NOP
NOP
L_flush_TX_RX22:
SUBS	R7, R7, #1
BNE	L_flush_TX_RX22
NOP
NOP
NOP
NOP
;tx.c,119 :: 		write_command(FLUSH_RX_cmd);
MOVS	R0, #226
BL	_write_command+0
;tx.c,120 :: 		}
L_end_flush_TX_RX:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _flush_TX_RX
_get_Status_Reg:
;tx.c,122 :: 		void get_Status_Reg()
SUB	SP, SP, #8
STR	LR, [SP, #0]
;tx.c,124 :: 		CSN_pin = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOE_ODR+0)
MOVT	R0, #hi_addr(GPIOE_ODR+0)
STR	R1, [R0, #0]
;tx.c,125 :: 		delay_ms(10);
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_get_Status_Reg24:
SUBS	R7, R7, #1
BNE	L_get_Status_Reg24
NOP
NOP
NOP
NOP
;tx.c,126 :: 		status= SPI1_Read(STATUS_reg);
MOVS	R0, #7
BL	_SPI1_Read+0
; status start address is: 28 (R7)
UXTH	R7, R0
;tx.c,128 :: 		CSN_pin = 1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOE_ODR+0)
MOVT	R0, #hi_addr(GPIOE_ODR+0)
STR	R1, [R0, #0]
;tx.c,129 :: 		delay_us(10);
STRH	R7, [SP, #4]
MOVW	R7, #51
MOVT	R7, #0
NOP
NOP
L_get_Status_Reg26:
SUBS	R7, R7, #1
BNE	L_get_Status_Reg26
NOP
NOP
NOP
NOP
LDRH	R7, [SP, #4]
;tx.c,130 :: 		src="Status Register = ";
MOVW	R1, #lo_addr(?lstr5_tx+0)
MOVT	R1, #hi_addr(?lstr5_tx+0)
MOVW	R0, #lo_addr(_src+0)
MOVT	R0, #hi_addr(_src+0)
STR	R1, [R0, #0]
;tx.c,131 :: 		UART1_Write_Text(src);
LDR	R0, [R0, #0]
BL	_UART1_Write_Text+0
;tx.c,132 :: 		delay_ms(10);
STRH	R7, [SP, #4]
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_get_Status_Reg28:
SUBS	R7, R7, #1
BNE	L_get_Status_Reg28
NOP
NOP
NOP
NOP
LDRH	R7, [SP, #4]
;tx.c,134 :: 		UART1_Write(status);
UXTH	R0, R7
; status end address is: 28 (R7)
BL	_UART1_Write+0
;tx.c,135 :: 		UART1_Write(10);
MOVS	R0, #10
BL	_UART1_Write+0
;tx.c,136 :: 		UART1_Write(10);
MOVS	R0, #10
BL	_UART1_Write+0
;tx.c,137 :: 		delay_ms(10);
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_get_Status_Reg30:
SUBS	R7, R7, #1
BNE	L_get_Status_Reg30
NOP
NOP
NOP
NOP
;tx.c,140 :: 		}
L_end_get_Status_Reg:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _get_Status_Reg
_set_TX_RX_address:
;tx.c,142 :: 		void set_TX_RX_address(unsigned char *addr, unsigned char bytes, unsigned char reg)
; reg start address is: 8 (R2)
; bytes start address is: 4 (R1)
; addr start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
MOV	R5, R0
UXTB	R6, R1
; reg end address is: 8 (R2)
; bytes end address is: 4 (R1)
; addr end address is: 0 (R0)
; addr start address is: 20 (R5)
; bytes start address is: 24 (R6)
; reg start address is: 8 (R2)
;tx.c,144 :: 		unsigned char n = 0;
;tx.c,146 :: 		CSN_pin= 0;
MOVS	R4, #0
SXTB	R4, R4
MOVW	R3, #lo_addr(GPIOE_ODR+0)
MOVT	R3, #hi_addr(GPIOE_ODR+0)
STR	R4, [R3, #0]
;tx.c,147 :: 		SPI1_write((reg | W_REGISTER_cmd));
ORR	R3, R2, #32
UXTB	R3, R3
; reg end address is: 8 (R2)
UXTB	R0, R3
BL	_SPI1_Write+0
;tx.c,148 :: 		for(n = 0; n < bytes; n++)
; n start address is: 0 (R0)
MOVS	R0, #0
; bytes end address is: 24 (R6)
; n end address is: 0 (R0)
UXTB	R4, R6
UXTB	R6, R0
L_set_TX_RX_address32:
; n start address is: 24 (R6)
; bytes start address is: 16 (R4)
; addr start address is: 20 (R5)
; addr end address is: 20 (R5)
CMP	R6, R4
IT	CS
BCS	L_set_TX_RX_address33
; addr end address is: 20 (R5)
;tx.c,150 :: 		SPI1_write(addr[n]);
; addr start address is: 20 (R5)
ADDS	R3, R5, R6
LDRB	R3, [R3, #0]
UXTH	R0, R3
BL	_SPI1_Write+0
;tx.c,148 :: 		for(n = 0; n < bytes; n++)
ADDS	R6, R6, #1
UXTB	R6, R6
;tx.c,151 :: 		}
; bytes end address is: 16 (R4)
; addr end address is: 20 (R5)
; n end address is: 24 (R6)
IT	AL
BAL	L_set_TX_RX_address32
L_set_TX_RX_address33:
;tx.c,152 :: 		CSN_pin = 1;
MOVS	R4, #1
SXTB	R4, R4
MOVW	R3, #lo_addr(GPIOE_ODR+0)
MOVT	R3, #hi_addr(GPIOE_ODR+0)
STR	R4, [R3, #0]
;tx.c,153 :: 		delay_us(8);
MOVW	R7, #41
MOVT	R7, #0
NOP
NOP
L_set_TX_RX_address35:
SUBS	R7, R7, #1
BNE	L_set_TX_RX_address35
NOP
NOP
;tx.c,154 :: 		}
L_end_set_TX_RX_address:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _set_TX_RX_address
_send_data:
;tx.c,178 :: 		void send_data(unsigned int k)
SUB	SP, SP, #8
STR	LR, [SP, #0]
STRH	R0, [SP, #4]
;tx.c,182 :: 		flush_TX_RX();
BL	_flush_TX_RX+0
;tx.c,184 :: 		CSN_pin= 0;
MOVS	R2, #0
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOE_ODR+0)
MOVT	R1, #hi_addr(GPIOE_ODR+0)
STR	R2, [R1, #0]
;tx.c,185 :: 		SPI1_write(W_TX_PAYLOAD_cmd);
MOVS	R0, #160
BL	_SPI1_Write+0
;tx.c,187 :: 		SPI1_write(k);
LDRH	R0, [SP, #4]
BL	_SPI1_Write+0
;tx.c,188 :: 		SPI1_write(k);
LDRH	R0, [SP, #4]
BL	_SPI1_Write+0
;tx.c,189 :: 		SPI1_write(k);
LDRH	R0, [SP, #4]
BL	_SPI1_Write+0
;tx.c,191 :: 		CSN_pin= 1;
MOVS	R2, #1
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOE_ODR+0)
MOVT	R1, #hi_addr(GPIOE_ODR+0)
STR	R2, [R1, #0]
;tx.c,192 :: 		delay_us(8);
MOVW	R7, #41
MOVT	R7, #0
NOP
NOP
L_send_data37:
SUBS	R7, R7, #1
BNE	L_send_data37
NOP
NOP
;tx.c,194 :: 		CE_pin = 1;
MOVS	R2, #1
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOA_ODR+0)
MOVT	R1, #hi_addr(GPIOA_ODR+0)
STR	R2, [R1, #0]
;tx.c,195 :: 		delay_us(60);
MOVW	R7, #318
MOVT	R7, #0
NOP
NOP
L_send_data39:
SUBS	R7, R7, #1
BNE	L_send_data39
NOP
NOP
NOP
;tx.c,196 :: 		CE_pin = 0;
MOVS	R2, #0
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOA_ODR+0)
MOVT	R1, #hi_addr(GPIOA_ODR+0)
STR	R2, [R1, #0]
;tx.c,198 :: 		}
L_end_send_data:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _send_data
_nrF24L01_init_TX:
;tx.c,201 :: 		void nrF24L01_init_TX()
SUB	SP, SP, #12
STR	LR, [SP, #0]
;tx.c,203 :: 		unsigned char address[5] = {0x78, 0x78, 0x78, 0x78, 0x78};
MOVS	R0, #120
STRB	R0, [SP, #4]
MOVS	R0, #120
STRB	R0, [SP, #5]
MOVS	R0, #120
STRB	R0, [SP, #6]
MOVS	R0, #120
STRB	R0, [SP, #7]
MOVS	R0, #120
STRB	R0, [SP, #8]
;tx.c,205 :: 		register_write(CONFIG_reg, 0x0E); // pwr_up =1 ,  enable CRC(2 bytes),  Prim:TX
MOVS	R1, #14
MOVS	R0, #0
BL	_register_write+0
;tx.c,207 :: 		set_TX_RX_address(address, 5, TX_ADDR_reg);
ADD	R0, SP, #4
MOVS	R2, #16
MOVS	R1, #5
BL	_set_TX_RX_address+0
;tx.c,209 :: 		set_TX_RX_address(address, 5, RX_ADDR_P0_reg);
ADD	R0, SP, #4
MOVS	R2, #10
MOVS	R1, #5
BL	_set_TX_RX_address+0
;tx.c,211 :: 		register_write(EN_AA_reg,0x01);
MOVS	R1, #1
MOVS	R0, #1
BL	_register_write+0
;tx.c,213 :: 		register_write(SETUP_RETR_reg,0x1A);
MOVS	R1, #26
MOVS	R0, #4
BL	_register_write+0
;tx.c,216 :: 		}
L_end_nrF24L01_init_TX:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _nrF24L01_init_TX
_main:
;tx.c,219 :: 		void main() {
;tx.c,220 :: 		SPI1_Init();
BL	_SPI1_Init+0
;tx.c,221 :: 		UART1_Init_Advanced(9600,_UART_8_BIT_DATA, _UART_NOPARITY, _UART_ONE_STOPBIT, &_GPIO_MODULE_USART1_PB67);
MOVW	R0, #lo_addr(__GPIO_MODULE_USART1_PB67+0)
MOVT	R0, #hi_addr(__GPIO_MODULE_USART1_PB67+0)
PUSH	(R0)
MOVW	R3, #0
MOVW	R2, #0
MOVW	R1, #0
MOVW	R0, #9600
BL	_UART1_Init_Advanced+0
ADD	SP, SP, #4
;tx.c,222 :: 		GPIO_Digital_Output(&GPIOD_BASE, _GPIO_PINMASK_12 );
MOVW	R1, #4096
MOVW	R0, #lo_addr(GPIOD_BASE+0)
MOVT	R0, #hi_addr(GPIOD_BASE+0)
BL	_GPIO_Digital_Output+0
;tx.c,223 :: 		GPIO_Digital_Output(&GPIOA_BASE, _GPIO_PINMASK_1 |_GPIO_PINMASK_5|_GPIO_PINMASK_7);
MOVS	R1, #162
MOVW	R0, #lo_addr(GPIOA_BASE+0)
MOVT	R0, #hi_addr(GPIOA_BASE+0)
BL	_GPIO_Digital_Output+0
;tx.c,224 :: 		GPIO_Digital_Output(&GPIOE_BASE, _GPIO_PINMASK_3);
MOVW	R1, #8
MOVW	R0, #lo_addr(GPIOE_BASE+0)
MOVT	R0, #hi_addr(GPIOE_BASE+0)
BL	_GPIO_Digital_Output+0
;tx.c,225 :: 		GPIO_Digital_Input(&GPIOA_BASE, _GPIO_PINMASK_6);
MOVW	R1, #64
MOVW	R0, #lo_addr(GPIOA_BASE+0)
MOVT	R0, #hi_addr(GPIOA_BASE+0)
BL	_GPIO_Digital_Input+0
;tx.c,226 :: 		GPIO_Digital_Input(&GPIOA_IDR, _GPIO_PINMASK_0);
MOVW	R1, #1
MOVW	R0, #lo_addr(GPIOA_IDR+0)
MOVT	R0, #hi_addr(GPIOA_IDR+0)
BL	_GPIO_Digital_Input+0
;tx.c,229 :: 		src="NRF-2016";
MOVW	R1, #lo_addr(?lstr6_tx+0)
MOVT	R1, #hi_addr(?lstr6_tx+0)
MOVW	R0, #lo_addr(_src+0)
MOVT	R0, #hi_addr(_src+0)
STR	R1, [R0, #0]
;tx.c,230 :: 		UART1_Write_Text(src);
LDR	R0, [R0, #0]
BL	_UART1_Write_Text+0
;tx.c,231 :: 		delay_ms(10);
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_main41:
SUBS	R7, R7, #1
BNE	L_main41
NOP
NOP
NOP
NOP
;tx.c,232 :: 		UART1_Write(10);
MOVS	R0, #10
BL	_UART1_Write+0
;tx.c,233 :: 		delay_ms(10);
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_main43:
SUBS	R7, R7, #1
BNE	L_main43
NOP
NOP
NOP
NOP
;tx.c,236 :: 		CE_pin = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOA_ODR+0)
MOVT	R0, #hi_addr(GPIOA_ODR+0)
STR	R1, [R0, #0]
;tx.c,237 :: 		CSN_pin = 0;
MOVW	R0, #lo_addr(GPIOE_ODR+0)
MOVT	R0, #hi_addr(GPIOE_ODR+0)
STR	R1, [R0, #0]
;tx.c,238 :: 		SCK_pin = 0;
MOVW	R0, #lo_addr(GPIOA_ODR+0)
MOVT	R0, #hi_addr(GPIOA_ODR+0)
STR	R1, [R0, #0]
;tx.c,239 :: 		MOSI_pin = 0;
MOVW	R0, #lo_addr(GPIOA_ODR+0)
MOVT	R0, #hi_addr(GPIOA_ODR+0)
STR	R1, [R0, #0]
;tx.c,240 :: 		delay_ms(900);
MOVW	R7, #15870
MOVT	R7, #73
NOP
NOP
L_main45:
SUBS	R7, R7, #1
BNE	L_main45
NOP
NOP
NOP
;tx.c,241 :: 		nrF24L01_init_TX();
BL	_nrF24L01_init_TX+0
;tx.c,242 :: 		delay_ms(1000);
MOVW	R7, #24915
MOVT	R7, #81
NOP
NOP
L_main47:
SUBS	R7, R7, #1
BNE	L_main47
NOP
NOP
NOP
NOP
;tx.c,244 :: 		while(1)
L_main49:
;tx.c,246 :: 		send_data(30);
MOVS	R0, #30
BL	_send_data+0
;tx.c,248 :: 		delay_ms(1500);
MOVW	R7, #4606
MOVT	R7, #122
NOP
NOP
L_main51:
SUBS	R7, R7, #1
BNE	L_main51
NOP
NOP
NOP
;tx.c,255 :: 		}
IT	AL
BAL	L_main49
;tx.c,257 :: 		}
L_end_main:
L__main_end_loop:
B	L__main_end_loop
; end of _main
