_register_write:
;nrf-stm-rx.c,53 :: 		void register_write(unsigned int reg, unsigned int value)
SUB	SP, SP, #20
STR	LR, [SP, #0]
STRH	R0, [SP, #4]
STRH	R1, [SP, #8]
;nrf-stm-rx.c,56 :: 		SPI1_Init();
BL	_SPI1_Init+0
;nrf-stm-rx.c,57 :: 		CSN_pin = 0;
MOVS	R3, #0
SXTB	R3, R3
MOVW	R2, #lo_addr(GPIOE_ODR+0)
MOVT	R2, #hi_addr(GPIOE_ODR+0)
STR	R3, [R2, #0]
;nrf-stm-rx.c,58 :: 		SPI1_Write((W_REGISTER_cmd|reg ));   //  YAZMA OLACAGI SOYLENIP- HANGI REG OLD. BELİRTILDI
LDRH	R2, [SP, #4]
ORR	R2, R2, #32
UXTH	R0, R2
BL	_SPI1_Write+0
;nrf-stm-rx.c,59 :: 		SPI1_Write(value);                //   ISTENEN DEGER YAZILDI.
LDRH	R0, [SP, #8]
BL	_SPI1_Write+0
;nrf-stm-rx.c,60 :: 		CSN_pin = 1;
MOVS	R3, #1
SXTB	R3, R3
MOVW	R2, #lo_addr(GPIOE_ODR+0)
MOVT	R2, #hi_addr(GPIOE_ODR+0)
STR	R3, [R2, #0]
;nrf-stm-rx.c,63 :: 		src="Yazilan Register= "; // delay silindi
MOVW	R3, #lo_addr(?lstr2_nrf_45stm_45rx+0)
MOVT	R3, #hi_addr(?lstr2_nrf_45stm_45rx+0)
MOVW	R2, #lo_addr(_src+0)
MOVT	R2, #hi_addr(_src+0)
STR	R2, [SP, #16]
STR	R3, [R2, #0]
;nrf-stm-rx.c,64 :: 		UART1_Write_Text(src);
LDR	R2, [R2, #0]
MOV	R0, R2
BL	_UART1_Write_Text+0
;nrf-stm-rx.c,65 :: 		delay_ms(10);
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_register_write0:
SUBS	R7, R7, #1
BNE	L_register_write0
NOP
NOP
NOP
NOP
;nrf-stm-rx.c,66 :: 		UART1_Write(reg);
LDRH	R0, [SP, #4]
BL	_UART1_Write+0
;nrf-stm-rx.c,67 :: 		UART1_Write(10);
MOVS	R0, #10
BL	_UART1_Write+0
;nrf-stm-rx.c,68 :: 		delay_ms(10);
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_register_write2:
SUBS	R7, R7, #1
BNE	L_register_write2
NOP
NOP
NOP
NOP
;nrf-stm-rx.c,69 :: 		src="Yazilan Deger= "; // delay silindi
MOVW	R3, #lo_addr(?lstr3_nrf_45stm_45rx+0)
MOVT	R3, #hi_addr(?lstr3_nrf_45stm_45rx+0)
MOVW	R2, #lo_addr(_src+0)
MOVT	R2, #hi_addr(_src+0)
STR	R3, [R2, #0]
;nrf-stm-rx.c,70 :: 		UART1_Write_Text(src);
LDR	R2, [SP, #16]
LDR	R2, [R2, #0]
MOV	R0, R2
BL	_UART1_Write_Text+0
;nrf-stm-rx.c,71 :: 		delay_ms(10);
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_register_write4:
SUBS	R7, R7, #1
BNE	L_register_write4
NOP
NOP
NOP
NOP
;nrf-stm-rx.c,72 :: 		UART1_Write(value);
LDRH	R0, [SP, #8]
BL	_UART1_Write+0
;nrf-stm-rx.c,73 :: 		UART1_Write(10);
MOVS	R0, #10
BL	_UART1_Write+0
;nrf-stm-rx.c,74 :: 		delay_ms(10);
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_register_write6:
SUBS	R7, R7, #1
BNE	L_register_write6
NOP
NOP
NOP
NOP
;nrf-stm-rx.c,76 :: 		}
L_end_register_write:
LDR	LR, [SP, #0]
ADD	SP, SP, #20
BX	LR
; end of _register_write
_register_read:
;nrf-stm-rx.c,78 :: 		unsigned char register_read(unsigned int reg)
; reg start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
UXTH	R7, R0
; reg end address is: 0 (R0)
; reg start address is: 28 (R7)
;nrf-stm-rx.c,80 :: 		unsigned int value = 0;
;nrf-stm-rx.c,82 :: 		src="Register Oku ="; // delay silindi
MOVW	R2, #lo_addr(?lstr4_nrf_45stm_45rx+0)
MOVT	R2, #hi_addr(?lstr4_nrf_45stm_45rx+0)
MOVW	R1, #lo_addr(_src+0)
MOVT	R1, #hi_addr(_src+0)
STR	R2, [R1, #0]
;nrf-stm-rx.c,83 :: 		UART1_Write_Text(src);
LDR	R1, [R1, #0]
MOV	R0, R1
BL	_UART1_Write_Text+0
;nrf-stm-rx.c,84 :: 		delay_ms(10);
STRH	R7, [SP, #4]
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_register_read8:
SUBS	R7, R7, #1
BNE	L_register_read8
NOP
NOP
NOP
NOP
LDRH	R7, [SP, #4]
;nrf-stm-rx.c,86 :: 		UART1_Write(reg);
UXTH	R0, R7
BL	_UART1_Write+0
;nrf-stm-rx.c,87 :: 		delay_ms(10);
STRH	R7, [SP, #4]
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_register_read10:
SUBS	R7, R7, #1
BNE	L_register_read10
NOP
NOP
NOP
NOP
LDRH	R7, [SP, #4]
;nrf-stm-rx.c,88 :: 		UART1_Write(10);
MOVS	R0, #10
BL	_UART1_Write+0
;nrf-stm-rx.c,89 :: 		delay_ms(10);
STRH	R7, [SP, #4]
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_register_read12:
SUBS	R7, R7, #1
BNE	L_register_read12
NOP
NOP
NOP
NOP
;nrf-stm-rx.c,93 :: 		delay_ms(10);
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_register_read14:
SUBS	R7, R7, #1
BNE	L_register_read14
NOP
NOP
NOP
NOP
;nrf-stm-rx.c,89 :: 		delay_ms(10);
LDRH	R7, [SP, #4]
;nrf-stm-rx.c,94 :: 		CSN_pin= 0;
MOVS	R2, #0
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOE_ODR+0)
MOVT	R1, #hi_addr(GPIOE_ODR+0)
STR	R2, [R1, #0]
;nrf-stm-rx.c,95 :: 		SPI1_Write((reg | R_REGISTER_cmd));
UXTH	R0, R7
; reg end address is: 28 (R7)
BL	_SPI1_Write+0
;nrf-stm-rx.c,96 :: 		value = Spi1_Read(0);
MOVS	R0, #0
BL	_SPI1_Read+0
; value start address is: 16 (R4)
UXTH	R4, R0
;nrf-stm-rx.c,97 :: 		delay_us(10);
MOVW	R7, #51
MOVT	R7, #0
NOP
NOP
L_register_read16:
SUBS	R7, R7, #1
BNE	L_register_read16
NOP
NOP
NOP
NOP
;nrf-stm-rx.c,98 :: 		CSN_pin= 1;
MOVS	R2, #1
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOE_ODR+0)
MOVT	R1, #hi_addr(GPIOE_ODR+0)
STR	R2, [R1, #0]
;nrf-stm-rx.c,99 :: 		delay_us(8);
MOVW	R7, #41
MOVT	R7, #0
NOP
NOP
L_register_read18:
SUBS	R7, R7, #1
BNE	L_register_read18
NOP
NOP
;nrf-stm-rx.c,101 :: 		UART1_Write(value);
UXTH	R0, R4
BL	_UART1_Write+0
;nrf-stm-rx.c,102 :: 		UART1_Write(10);
MOVS	R0, #10
BL	_UART1_Write+0
;nrf-stm-rx.c,103 :: 		return value;
UXTB	R0, R4
; value end address is: 16 (R4)
;nrf-stm-rx.c,104 :: 		}
L_end_register_read:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _register_read
_write_command:
;nrf-stm-rx.c,106 :: 		void write_command(unsigned int cmd)
; cmd start address is: 0 (R0)
SUB	SP, SP, #8
STR	LR, [SP, #0]
; cmd end address is: 0 (R0)
; cmd start address is: 0 (R0)
;nrf-stm-rx.c,107 :: 		{ SPI1_Init();
STRH	R0, [SP, #4]
BL	_SPI1_Init+0
LDRH	R0, [SP, #4]
;nrf-stm-rx.c,108 :: 		CSN_pin = 0;
MOVS	R2, #0
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOE_ODR+0)
MOVT	R1, #hi_addr(GPIOE_ODR+0)
STR	R2, [R1, #0]
;nrf-stm-rx.c,109 :: 		SPI1_Write(cmd);
; cmd end address is: 0 (R0)
BL	_SPI1_Write+0
;nrf-stm-rx.c,110 :: 		CSN_pin = 1;
MOVS	R2, #1
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOE_ODR+0)
MOVT	R1, #hi_addr(GPIOE_ODR+0)
STR	R2, [R1, #0]
;nrf-stm-rx.c,111 :: 		delay_us(8);
MOVW	R7, #41
MOVT	R7, #0
NOP
NOP
L_write_command20:
SUBS	R7, R7, #1
BNE	L_write_command20
NOP
NOP
;nrf-stm-rx.c,112 :: 		}
L_end_write_command:
LDR	LR, [SP, #0]
ADD	SP, SP, #8
BX	LR
; end of _write_command
_flush_TX_RX:
;nrf-stm-rx.c,114 :: 		void flush_TX_RX()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;nrf-stm-rx.c,115 :: 		{ SPI1_Init();
BL	_SPI1_Init+0
;nrf-stm-rx.c,116 :: 		register_write(STATUS_reg,0x70);
MOVS	R1, #112
MOVS	R0, #7
BL	_register_write+0
;nrf-stm-rx.c,117 :: 		write_command(FLUSH_TX_cmd);
MOVS	R0, #225
BL	_write_command+0
;nrf-stm-rx.c,118 :: 		delay_us(10);
MOVW	R7, #51
MOVT	R7, #0
NOP
NOP
L_flush_TX_RX22:
SUBS	R7, R7, #1
BNE	L_flush_TX_RX22
NOP
NOP
NOP
NOP
;nrf-stm-rx.c,119 :: 		write_command(FLUSH_RX_cmd);
MOVS	R0, #226
BL	_write_command+0
;nrf-stm-rx.c,120 :: 		}
L_end_flush_TX_RX:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _flush_TX_RX
_get_Status_Reg:
;nrf-stm-rx.c,123 :: 		unsigned char get_Status_Reg()
SUB	SP, SP, #4
STR	LR, [SP, #0]
;nrf-stm-rx.c,125 :: 		return register_read(STATUS_reg);
MOVS	R0, #7
BL	_register_read+0
;nrf-stm-rx.c,126 :: 		}
L_end_get_Status_Reg:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _get_Status_Reg
_set_TX_RX_address:
;nrf-stm-rx.c,149 :: 		void set_TX_RX_address(unsigned char *addr, unsigned char bytes, unsigned char reg)
; reg start address is: 8 (R2)
; bytes start address is: 4 (R1)
; addr start address is: 0 (R0)
SUB	SP, SP, #4
STR	LR, [SP, #0]
MOV	R5, R0
UXTB	R6, R1
; reg end address is: 8 (R2)
; bytes end address is: 4 (R1)
; addr end address is: 0 (R0)
; addr start address is: 20 (R5)
; bytes start address is: 24 (R6)
; reg start address is: 8 (R2)
;nrf-stm-rx.c,151 :: 		unsigned char n = 0;
;nrf-stm-rx.c,153 :: 		CSN_pin= 0;
MOVS	R4, #0
SXTB	R4, R4
MOVW	R3, #lo_addr(GPIOE_ODR+0)
MOVT	R3, #hi_addr(GPIOE_ODR+0)
STR	R4, [R3, #0]
;nrf-stm-rx.c,154 :: 		SPI1_write((reg | W_REGISTER_cmd));
ORR	R3, R2, #32
UXTB	R3, R3
; reg end address is: 8 (R2)
UXTB	R0, R3
BL	_SPI1_Write+0
;nrf-stm-rx.c,155 :: 		for(n = 0; n < bytes; n++)
; n start address is: 0 (R0)
MOVS	R0, #0
; bytes end address is: 24 (R6)
; n end address is: 0 (R0)
UXTB	R4, R6
UXTB	R6, R0
L_set_TX_RX_address24:
; n start address is: 24 (R6)
; bytes start address is: 16 (R4)
; addr start address is: 20 (R5)
; addr end address is: 20 (R5)
CMP	R6, R4
IT	CS
BCS	L_set_TX_RX_address25
; addr end address is: 20 (R5)
;nrf-stm-rx.c,157 :: 		SPI1_write(addr[n]);
; addr start address is: 20 (R5)
ADDS	R3, R5, R6
LDRB	R3, [R3, #0]
UXTH	R0, R3
BL	_SPI1_Write+0
;nrf-stm-rx.c,155 :: 		for(n = 0; n < bytes; n++)
ADDS	R6, R6, #1
UXTB	R6, R6
;nrf-stm-rx.c,158 :: 		}
; bytes end address is: 16 (R4)
; addr end address is: 20 (R5)
; n end address is: 24 (R6)
IT	AL
BAL	L_set_TX_RX_address24
L_set_TX_RX_address25:
;nrf-stm-rx.c,159 :: 		CSN_pin = 1;
MOVS	R4, #1
SXTB	R4, R4
MOVW	R3, #lo_addr(GPIOE_ODR+0)
MOVT	R3, #hi_addr(GPIOE_ODR+0)
STR	R4, [R3, #0]
;nrf-stm-rx.c,160 :: 		delay_us(8);
MOVW	R7, #41
MOVT	R7, #0
NOP
NOP
L_set_TX_RX_address27:
SUBS	R7, R7, #1
BNE	L_set_TX_RX_address27
NOP
NOP
;nrf-stm-rx.c,161 :: 		}
L_end_set_TX_RX_address:
LDR	LR, [SP, #0]
ADD	SP, SP, #4
BX	LR
; end of _set_TX_RX_address
_nrF24L01_init_RX:
;nrf-stm-rx.c,166 :: 		void nrF24L01_init_RX()
SUB	SP, SP, #12
STR	LR, [SP, #0]
;nrf-stm-rx.c,168 :: 		unsigned char address[5] = {0x78, 0x78, 0x78, 0x78, 0x78};
MOVS	R0, #120
STRB	R0, [SP, #4]
MOVS	R0, #120
STRB	R0, [SP, #5]
MOVS	R0, #120
STRB	R0, [SP, #6]
MOVS	R0, #120
STRB	R0, [SP, #7]
MOVS	R0, #120
STRB	R0, [SP, #8]
;nrf-stm-rx.c,169 :: 		flush_TX_RX();
BL	_flush_TX_RX+0
;nrf-stm-rx.c,171 :: 		register_write(CONFIG_reg, 0x0F);   // Set PWR_UP bit, enable CRC(2 bytes) & Prim:RX. RX_DR enabled..
MOVS	R1, #15
MOVS	R0, #0
BL	_register_write+0
;nrf-stm-rx.c,173 :: 		register_write(EN_RXADDR_reg, 0x01);
MOVS	R1, #1
MOVS	R0, #2
BL	_register_write+0
;nrf-stm-rx.c,175 :: 		register_write(EN_AA_reg,0x01);
MOVS	R1, #1
MOVS	R0, #1
BL	_register_write+0
;nrf-stm-rx.c,177 :: 		register_write(RX_PW_P0_reg,0x03);
MOVS	R1, #3
MOVS	R0, #17
BL	_register_write+0
;nrf-stm-rx.c,179 :: 		set_TX_RX_address(address, 5, RX_ADDR_P0_reg);
ADD	R0, SP, #4
MOVS	R2, #10
MOVS	R1, #5
BL	_set_TX_RX_address+0
;nrf-stm-rx.c,181 :: 		CE_pin=1;
MOVS	R1, #1
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOA_ODR+0)
MOVT	R0, #hi_addr(GPIOA_ODR+0)
STR	R1, [R0, #0]
;nrf-stm-rx.c,182 :: 		delay_ms(10);
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_nrF24L01_init_RX29:
SUBS	R7, R7, #1
BNE	L_nrF24L01_init_RX29
NOP
NOP
NOP
NOP
;nrf-stm-rx.c,183 :: 		}
L_end_nrF24L01_init_RX:
LDR	LR, [SP, #0]
ADD	SP, SP, #12
BX	LR
; end of _nrF24L01_init_RX
_main:
;nrf-stm-rx.c,201 :: 		void main() {
;nrf-stm-rx.c,202 :: 		SPI1_Init();
BL	_SPI1_Init+0
;nrf-stm-rx.c,203 :: 		UART1_Init_Advanced(9600,_UART_8_BIT_DATA, _UART_NOPARITY, _UART_ONE_STOPBIT, &_GPIO_MODULE_USART1_PB67);
MOVW	R0, #lo_addr(__GPIO_MODULE_USART1_PB67+0)
MOVT	R0, #hi_addr(__GPIO_MODULE_USART1_PB67+0)
PUSH	(R0)
MOVW	R3, #0
MOVW	R2, #0
MOVW	R1, #0
MOVW	R0, #9600
BL	_UART1_Init_Advanced+0
ADD	SP, SP, #4
;nrf-stm-rx.c,204 :: 		GPIO_Digital_Output(&GPIOD_BASE, _GPIO_PINMASK_12 );
MOVW	R1, #4096
MOVW	R0, #lo_addr(GPIOD_BASE+0)
MOVT	R0, #hi_addr(GPIOD_BASE+0)
BL	_GPIO_Digital_Output+0
;nrf-stm-rx.c,205 :: 		GPIO_Digital_Output(&GPIOA_BASE, _GPIO_PINMASK_1 |_GPIO_PINMASK_5|_GPIO_PINMASK_7);
MOVS	R1, #162
MOVW	R0, #lo_addr(GPIOA_BASE+0)
MOVT	R0, #hi_addr(GPIOA_BASE+0)
BL	_GPIO_Digital_Output+0
;nrf-stm-rx.c,206 :: 		GPIO_Digital_Output(&GPIOE_BASE, _GPIO_PINMASK_3);
MOVW	R1, #8
MOVW	R0, #lo_addr(GPIOE_BASE+0)
MOVT	R0, #hi_addr(GPIOE_BASE+0)
BL	_GPIO_Digital_Output+0
;nrf-stm-rx.c,207 :: 		GPIO_Digital_Input(&GPIOA_BASE, _GPIO_PINMASK_6);
MOVW	R1, #64
MOVW	R0, #lo_addr(GPIOA_BASE+0)
MOVT	R0, #hi_addr(GPIOA_BASE+0)
BL	_GPIO_Digital_Input+0
;nrf-stm-rx.c,208 :: 		GPIO_Digital_Input(&GPIOA_IDR, _GPIO_PINMASK_0);
MOVW	R1, #1
MOVW	R0, #lo_addr(GPIOA_IDR+0)
MOVT	R0, #hi_addr(GPIOA_IDR+0)
BL	_GPIO_Digital_Input+0
;nrf-stm-rx.c,211 :: 		src="NRF-2016";
MOVW	R1, #lo_addr(?lstr5_nrf_45stm_45rx+0)
MOVT	R1, #hi_addr(?lstr5_nrf_45stm_45rx+0)
MOVW	R0, #lo_addr(_src+0)
MOVT	R0, #hi_addr(_src+0)
STR	R1, [R0, #0]
;nrf-stm-rx.c,212 :: 		UART1_Write_Text(src);
LDR	R0, [R0, #0]
BL	_UART1_Write_Text+0
;nrf-stm-rx.c,213 :: 		delay_ms(10);
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_main31:
SUBS	R7, R7, #1
BNE	L_main31
NOP
NOP
NOP
NOP
;nrf-stm-rx.c,214 :: 		UART1_Write(10);
MOVS	R0, #10
BL	_UART1_Write+0
;nrf-stm-rx.c,215 :: 		delay_ms(10);
MOVW	R7, #53331
MOVT	R7, #0
NOP
NOP
L_main33:
SUBS	R7, R7, #1
BNE	L_main33
NOP
NOP
NOP
NOP
;nrf-stm-rx.c,217 :: 		CSN_pin = 0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOE_ODR+0)
MOVT	R0, #hi_addr(GPIOE_ODR+0)
STR	R1, [R0, #0]
;nrf-stm-rx.c,218 :: 		SCK_pin = 0;
MOVW	R0, #lo_addr(GPIOA_ODR+0)
MOVT	R0, #hi_addr(GPIOA_ODR+0)
STR	R1, [R0, #0]
;nrf-stm-rx.c,219 :: 		MOSI_pin = 0;
MOVW	R0, #lo_addr(GPIOA_ODR+0)
MOVT	R0, #hi_addr(GPIOA_ODR+0)
STR	R1, [R0, #0]
;nrf-stm-rx.c,220 :: 		delay_ms(900);
MOVW	R7, #15870
MOVT	R7, #73
NOP
NOP
L_main35:
SUBS	R7, R7, #1
BNE	L_main35
NOP
NOP
NOP
;nrf-stm-rx.c,221 :: 		nrF24L01_init_RX();
BL	_nrF24L01_init_RX+0
;nrf-stm-rx.c,222 :: 		delay_ms(1000);
MOVW	R7, #24915
MOVT	R7, #81
NOP
NOP
L_main37:
SUBS	R7, R7, #1
BNE	L_main37
NOP
NOP
NOP
NOP
;nrf-stm-rx.c,224 :: 		while(1)
L_main39:
;nrf-stm-rx.c,226 :: 		if(get_Status_Reg() == 0x40)
BL	_get_Status_Reg+0
CMP	R0, #64
IT	NE
BNE	L_main41
;nrf-stm-rx.c,229 :: 		CSN_pin=0;
MOVS	R1, #0
SXTB	R1, R1
MOVW	R0, #lo_addr(GPIOE_ODR+0)
MOVT	R0, #hi_addr(GPIOE_ODR+0)
STR	R1, [R0, #0]
;nrf-stm-rx.c,232 :: 		delay_ms(19);
MOVW	R7, #35795
MOVT	R7, #1
NOP
NOP
L_main42:
SUBS	R7, R7, #1
BNE	L_main42
NOP
NOP
NOP
NOP
;nrf-stm-rx.c,233 :: 		SPI1_Write(0b01100001);
MOVS	R0, #97
BL	_SPI1_Write+0
;nrf-stm-rx.c,234 :: 		y=SPI1_Read(0);
MOVS	R0, #0
BL	_SPI1_Read+0
MOVW	R1, #lo_addr(_y+0)
MOVT	R1, #hi_addr(_y+0)
STRH	R0, [R1, #0]
;nrf-stm-rx.c,236 :: 		Led=1;
MOVS	R2, #1
SXTB	R2, R2
MOVW	R1, #lo_addr(GPIOD_ODR+0)
MOVT	R1, #hi_addr(GPIOD_ODR+0)
STR	R2, [R1, #0]
;nrf-stm-rx.c,237 :: 		UART1_Write(y);
BL	_UART1_Write+0
;nrf-stm-rx.c,238 :: 		delay_ms(500) ;
MOVW	R7, #45225
MOVT	R7, #40
NOP
NOP
L_main44:
SUBS	R7, R7, #1
BNE	L_main44
NOP
NOP
;nrf-stm-rx.c,239 :: 		}
L_main41:
;nrf-stm-rx.c,242 :: 		}
IT	AL
BAL	L_main39
;nrf-stm-rx.c,244 :: 		}
L_end_main:
L__main_end_loop:
B	L__main_end_loop
; end of _main
